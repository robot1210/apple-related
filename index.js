import fs from "fs";
import path from "path";
import dateFormat from "dateformat";

import fetch from "node-fetch";
import notifier from "node-notifier";

import GmailSender from "gmail-send";

const GMAIL_SENDER_OPT = {
  user: "robot1210@gmail.com", // Login user name & actual sender address
  pass: "Kurt1123@", // Login password
  from: "iPhone 13 Pro/Pro Max - AOS 店內取貨監測", // Display sender desc
  to: "robot1210@gmail.com", // recipient address 1
  // to: "tszting84@gmail.com", // Recipient address 2
  // cc: "robot1210@gmail.com", // CC address
};

const __dirname = path.resolve(path.dirname(""));

const TIME_UNIT_S = "second";
const TIME_UNIT_M = "minute";

//  "https://www.apple.com/hk/shop/fulfillment-messages?pl=true&mt=compact&parts.0=MLTJ3ZA/A&location=觀塘";
const FETCH_URL_PREFIX =
  "https://www.apple.com/hk-zh/shop/fulfillment-messages?pl=true&mt=compact&parts.0=";
const FETCH_URL_SUFFIX = "&location=觀塘";

let MODELS_DATA;

(function () {
  "use strict";
  readAllModelsJson();
  iterateModelDataAndFetch();
})();

function readAllModelsJson() {
  const modelsData = fs.readFileSync(
    path.join(__dirname, "./resources/json/tszting-target-models.json"),
    { encoding: "utf8", flag: "r" }
  );
  // console.debug(modelsData);

  MODELS_DATA = JSON.parse(modelsData);
}

async function iterateModelDataAndFetch() {
  await Promise.all(
    MODELS_DATA.map(async (modelObj) => {
      await Promise.all(
        modelObj.colors.map(async (colorObj) => {
          await Promise.all(
            colorObj.capacities.map(async (capacityObj) => {
              const finalUrl = `${FETCH_URL_PREFIX}${capacityObj.modelCode}${FETCH_URL_SUFFIX}`;
              // console.debug(`relative URL > ${finalUrl}`);
              const fetchResult = await startFetch(finalUrl);

              let modelAvailableMsgPrefix = `${modelObj.modelName} ${colorObj.name} ${capacityObj.capacity}`;
              // console.debug(
              //   `${modelAvailableMsgPrefix} isAvaiable > ${
              //     fetchResult ? "true" : "false"
              //   }`
              // );
              if (fetchResult) {
                const finalResultMsg = `${modelAvailableMsgPrefix} 可以在 ${fetchResult} (${dateFormat(
                  new Date(),
                  "yyyy-mm-dd HH:mm:ss TT"
                )})`;

                console.log(finalResultMsg);

                showWindowNoti("iPhone 13 Fetcher", finalResultMsg);
                sendGmail(finalResultMsg, finalResultMsg);
              }
            })
          );
        })
      );
    })
  );

  const randamTimeout = getRandomIntervalInclusive(1, 2, TIME_UNIT_S);
  console.log(`Check availability after ${randamTimeout / 1000} second(s)`);
  setTimeout(iterateModelDataAndFetch, randamTimeout);
}

const availableModelList = [];

async function startFetch(url) {
  // console.debug(url);
  return await fetch(url, {
    cache: "no-cache",
    credentials: "include",
    method: "GET",
    mode: "same-origin",
  })
    .then(async (response) => {
      if (response.ok) {
        var jsonPromise = response.json();
        return await jsonPromise.then(async (data) => {
          for (const store of data.body.content.pickupMessage.stores) {
            const storeName = store.storeName;
            // console.debug(storeName);
            for (let key in store.partsAvailability) {
              let partsNestedObj = store.partsAvailability[key];
              // console.debug(partsNestedObj.storePickupQuote);
              if (partsNestedObj.storeSelectionEnabled) {
                if (!availableModelList.includes(partsNestedObj.partNumber)) {
                  availableModelList.push(partsNestedObj.partNumber);
                }

                return `${partsNestedObj.storePickupQuote} ${partsNestedObj.pickupType}`;
              } else {
                const index = availableModelList.indexOf(
                  partsNestedObj.partNumber
                );
                if (index > -1) {
                  availableModelList.splice(index, 1);
                }
              }
            }
          }
          return "";
        });
      } else {
        console.error(
          "Error occurs when fetching iPhone availability, invalid HTTP response"
        );
      }
    })
    .catch((err) => {
      // console.error(err);
      console.error(
        "Error occurs when fetching iPhone availability:",
        err.message
      );
      // showWindowNoti("Error occurs when fetching info", err.message);
    });
}

function getRandomIntervalInclusive(min, max, timeUnit) {
  min = Math.ceil(min);
  max = Math.floor(max);

  let randomInt = Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive

  if (TIME_UNIT_S === timeUnit) {
    return randomInt * 1000;
  } else if (TIME_UNIT_M === timeUnit) {
    return randomInt * 1000 * 60;
  } else {
    return randomInt * 1000;
  }
}

function showWindowNoti(titleStr, contentStr) {
  new notifier.WindowsToaster().notify({
    appID: titleStr,
    title: titleStr,
    message: contentStr,
    icon: path.join(__dirname, "./resources/img/apple.png"),
    sound: false,
    wait: true,
  });
}

function appendExtraSpace2AlignResultMsg(resultMsg) {
  while (resultMsg.length < 35) {
    resultMsg += " ";
  }

  return resultMsg;
}

function sendGmail(titleStr, contentStr) {
  const textContent = contentStr;
  const htmlContent = `<html><body><h3>${contentStr}</h3></body></html>`;

  const GMAIL_SENDER_OPT_W_CONTENT = {
    ...GMAIL_SENDER_OPT,
    subject: titleStr,
    text: textContent,
  };

  const gmailSender = new GmailSender(GMAIL_SENDER_OPT_W_CONTENT);

  gmailSender()
    .then(({ result, full }) => {
      console.log(
        `Alert Email sent to: <${GMAIL_SENDER_OPT.to}>, cc: <${GMAIL_SENDER_OPT.cc}>`
      );
      // console.debug(result);
      // console.debug(full);
    })
    .catch((error) => {
      console.log("Cannot send email:", error);
    });
}
